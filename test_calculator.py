import unittest

from calculator import add

class TestAddition(unittest.TestCase):

    def test_positive_numbers(self):
        numA, numB = 3, 2
        result = 5
        self.assertEqual(add(numA, numB), result)

    def test_negative_number(self):
        numA, numB = 1, -3
        result = -2
        self.assertEqual(add(numA, numB), result)

    def test_invalid_input_string(self):
        numA, numB = 1, "hi"
        with self.assertRaises(ValueError):
            add(numA, numB)

if __name__ == '__main__':
    unittest.main()
